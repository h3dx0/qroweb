from qrofind_web.main.models import *

def get_zona_activa(request):
    zona = Zona.objects.get(slug=request.session['slug_zona_activa'])
    return zona