from django.apps import AppConfig


class NegociosConfig(AppConfig):
    name = 'qrofind_web.negocios'
