from django.shortcuts import render
from qrofind_web.negocios.forms import *
from django.core.mail import send_mail
from django.conf import settings
import datetime
from django.http import HttpResponse
from qrofind_web.negocios.utils import *
import json


# Create your views here.


def negocios_categorias(request, slug_categoria):
    zona = Zona.objects.get(slug=request.session['slug_zona_activa'])
    categoria = Categoria.objects.get(slug=slug_categoria, zona=zona)
    negocios_promocionados = None
    subcategorias = None
    if slug_categoria == "servicio-a-domicilio":
        negocios_promocionados = Negocio.objects.filter(promocionado=True,
                                                        zona=zona, servicio_domicilio=True).order_by('orden').distinct()
        subcategorias = Subcategoria.objects.filter(negocios__servicio_domicilio=True,categoria__zona=zona).order_by('orden').distinct()

    if slug_categoria != "servicio-a-domicilio":
        negocios_promocionados = Negocio.objects.filter(sub_categoria__categoria=categoria, promocionado=True,
                                                        zona=zona).order_by('orden').distinct()
        subcategorias = categoria.subcategorias.all().order_by('orden').distinct()
    context = {
        'categoria': categoria,
        'subcategorias': subcategorias,
        'negocios_promocionados': negocios_promocionados,
    }
    return render(request, 'negocio/categoria.html', context)


def detalle_negocio(request, slug_negocio):
    contacto_form = ContactoForm()
    promociones = None
    zona = Zona.objects.get(slug=request.session['slug_zona_activa'])
    negocio = Negocio.objects.get(slug=slug_negocio, zona=zona)
    promociones_generales = Promocion.objects.filter(negocio=negocio, fecha_inicio__lte=datetime.datetime.now(),
                                                     fecha_fin__gte=datetime.datetime.now()).order_by('?')[:1]
    mensaje_contacto = None
    # Registro de clic

    if promociones_generales.count() > 0:
        promociones = promociones_generales[0]
    if request.method == 'POST':
        contacto_form = ContactoForm(request.POST)
        if contacto_form.is_valid():
            nombre = contacto_form.cleaned_data['nombre']
            email = contacto_form.cleaned_data['email']
            negocio_contacto = negocio.email_contacto
            listado_correos = ["roberto.nunez@dgtalgroup.com", "jorge@dgtalgroup.com", negocio_contacto]

            if negocio.email_contacto2 != "":
                listado_correos.append(negocio.email_contacto2)

            if negocio.email_contacto3 != "":
                listado_correos.append(negocio.email_contacto3)

            mensaje = contacto_form.cleaned_data['mensaje']
            texto = 'De {} email: {} \n Mensjaje: {}'.format(nombre, email, mensaje)

            send_mail('Correo desde la web', texto, settings.DEFAULT_FROM_EMAIL,
                      listado_correos, fail_silently=True)

            mensaje_contacto = "Se ha enviado su mensaje"

            registro_contacto = RegistroContato()
            registro_contacto.negocio = negocio
            registro_contacto.correo = email
            registro_contacto.mensaje_de = nombre
            registro_contacto.texto = mensaje
            registro_contacto.save()
    context = {
        'negocio': negocio,
        'promociones_generales': promociones,
        'mensaje_contacto': mensaje_contacto,
        'contacto_form': contacto_form
    }
    return render(request, 'negocio/detalle_negocio.html', context)


def promociones(request):
    zona_activa = get_zona_activa(request)
    listado_promociones = Promocion.objects.filter(negocio__zona=zona_activa,
                                                   fecha_inicio__lte=datetime.datetime.now(),
                                                   fecha_fin__gte=datetime.datetime.now())
    banner = Banner.objects.filter(seccion=0).order_by('-orden')[:1]
    context = {'banner': banner, 'promociones': listado_promociones}
    return render(request, 'negocio/promociones.html', context)


def registro_clic_negocio(request):
    negocio_id = request.GET.get('negocio')
    negocio = Negocio.objects.get(pk=negocio_id)
    cantidad_clic = negocio.cantidad_clic
    nueva_cantidad = cantidad_clic + 1
    negocio.cantidad_clic = nueva_cantidad
    negocio.save()
    json_data = {'rc': 0}
    return HttpResponse(json.dumps(json_data), content_type='application/json')


def anunciate(request):
    context = get_anunciate_form(request)
    return render(request, 'negocio/anunciate.html', context)


def anunciate_inicio(request):
    zonas = Zona.objects.all().exclude(slug='default')[:7]

    context = get_anunciate_form(request)
    context['zonas'] = zonas
    return render(request, 'negocio/anunciate_home.html', context)


def get_anunciate_form(request):
    form = AnunciateForm()
    mensaje = None
    if request.method == 'POST':
        form = AnunciateForm(request.POST)
        if form.is_valid():
            nombre = form.cleaned_data['nombre']
            nombre_negocio = form.cleaned_data['nombre_negocio']
            email = form.cleaned_data['email']
            direccion = form.cleaned_data['direccion']
            telefono = form.cleaned_data['telefono']
            mensaje = form.cleaned_data['mensaje']
            texto = 'De {} email: {} \n nombre negocio {} \n Direccion {}' \
                    ' \n Teléfono {} \n Mensaje: {}'.format(nombre,
                                                            email,
                                                            nombre_negocio,
                                                            direccion,
                                                            telefono,
                                                            mensaje)
            send_mail('Correo desde la web para anunciante', texto, settings.DEFAULT_FROM_EMAIL,
                      ['pedro@tuzona.mx'], fail_silently=True)
            mensaje = "Se ha enviado su mensaje"

    return {'form': form, 'mensaje': mensaje}
