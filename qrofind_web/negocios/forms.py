from django import forms
from qrofind_web.negocios.models import *

class ContactoForm(forms.Form):

    nombre = forms.CharField(max_length=100, widget=forms.TextInput(attrs={'placeholder': 'Nombre'}))
    email = forms.EmailField()
    mensaje = forms.CharField(max_length=100, widget=forms.Textarea(attrs={'placeholder': 'Mensaje'}))


class AnunciateForm(forms.Form):

    nombre = forms.CharField(max_length=100, widget=forms.TextInput(attrs={'placeholder': 'Nombre'}))
    email = forms.EmailField()
    nombre_negocio = forms.CharField(max_length=100, widget=forms.TextInput(attrs={'placeholder': 'Nombre del negocio'}))
    telefono = forms.IntegerField(widget=forms.TextInput(attrs={'placeholder': 'Teléfono'}))
    direccion = forms.CharField(max_length=150, widget=forms.TextInput(attrs={'placeholder': 'Dirección'}))
    mensaje = forms.CharField(max_length=100, widget=forms.Textarea(attrs={'placeholder': 'Mensaje'}))



class CategoriaForm(forms.ModelForm):
    class Meta:
        model = Categoria
        fields = ['zona', 'nombre', 'orden', 'banner']
