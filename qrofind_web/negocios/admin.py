from django.contrib import admin
from .models import *
from qrofind_web.negocios.forms import *
from qrofind_web.main.models import *

class FotoNegocioAdmin(admin.TabularInline):
    model = FotoNegocio


class FotoNegocioAmpliaAdmin(admin.TabularInline):
    model = FotoNegocioAmplia


class NegocioAdmin(admin.ModelAdmin):
    list_filter = ['zona', 'sub_categoria']
    list_display = ['nombre', 'zona', 'cantidad_clic']
    inlines = [
        FotoNegocioAmpliaAdmin,
        FotoNegocioAdmin,
    ]

    #este es el metodo que se ejecuta al crear un objeto del modelo
    def get_form(self, request, obj=None, **kwargs):
        #crear el from limpio
        form = super(NegocioAdmin, self).get_form(request, obj=None, **kwargs)
        #si no es super usuario el q pide
        if request.user.is_superuser is False:
            #le aplicas un query especifico al campo zona para que solo cargue ciertos datos
            form.base_fields['zona'].queryset = Zona.objects.filter(
                pk=request.user.zona.pk)
            zona = Zona.objects.filter(pk=request.user.zona.pk)

            #le aplicas un query especifico al campo subcategoria para que solo cargue ciertos datos
            form.base_fields['sub_categoria'].queryset = Subcategoria.objects.filter(categoria__zona=zona).order_by('nombre')
        return form

    #este es el metodo que muesta el listado
    def get_queryset(self, request):
        qs = super(NegocioAdmin, self).get_queryset(request)
        #si es super usuario muestra  el query
        if request.user.is_superuser:
            qs = qs.all()
        else:
            #sino filtra el query con los argumentos que quieras del modelo
            zona = request.user.zona
            qs = qs.filter(zona=zona)
        return qs


class CategoriaAdmin(admin.ModelAdmin):
    list_display = ['nombre', 'zona']

    def get_form(self, request, obj=None, **kwargs):
        form = super(CategoriaAdmin, self).get_form(request, obj=None, **kwargs)
        if request.user.is_superuser is False:
            form.base_fields['zona'].queryset = Zona.objects.filter(
                pk=request.user.zona.pk)
        return form

    def get_queryset(self, request):

        qs = super(CategoriaAdmin, self).get_queryset(request)

        if request.user.is_superuser:
            qs = qs.all()
        else:
            zona = request.user.zona
            qs = qs.filter(zona=zona)
        return qs


class SubCategoriaAdmin(admin.ModelAdmin):
    list_display = ['nombre', 'categoria', 'get_zona', 'inicio']

    def get_form(self, request, obj=None, **kwargs):
        form = super(SubCategoriaAdmin, self).get_form(request, obj=None, **kwargs)
        if request.user.is_superuser is False:
            print(form.base_fields)
            form.base_fields['categoria'].queryset = Categoria.objects.filter(
                zona=request.user.zona.pk)
        return form

    def get_queryset(self, request):
        qs = super(SubCategoriaAdmin, self).get_queryset(request)

        if request.user.is_superuser:
            qs = qs.all().order_by('nombre')
        else:
            zona = request.user.zona
            qs = qs.filter(categoria__zona=zona).order_by('nombre')
        return qs


class PromocionAdmin(admin.ModelAdmin):

    def get_form(self, request, obj=None, **kwargs):
        form = super(PromocionAdmin, self).get_form(request, obj=None, **kwargs)
        if request.user.is_superuser is False:
            zona = Zona.objects.filter(pk=request.user.zona.pk)
            form.base_fields['negocio'].queryset = Negocio.objects.filter(zona=zona)

        return form

    def get_queryset(self, request):
        qs = super(PromocionAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            qs = qs.all()
        else:
            zona = request.user.zona
            qs = qs.filter(negocio__zona=zona)
        return qs

admin.site.register(Categoria, CategoriaAdmin)
admin.site.register(Subcategoria, SubCategoriaAdmin)
admin.site.register(Servicio)
admin.site.register(Promocion, PromocionAdmin)
admin.site.register(RegistroContato)
admin.site.register(Negocio, NegocioAdmin)
