/**
 * Created by h3dx0 on 14/10/16.
 */
$(function () {
    var currentdate = new Date();
    var datetime = currentdate.getMinutes();
    var current_hora = currentdate.getHours();
    var minuto_guardado = localStorage.getItem('min-inicio-detalle');
    var hora_guardada = localStorage.getItem('hora-inicio-detalle');
    var negocio_visto = localStorage.getItem('negocio-visto');
    var resta = datetime - minuto_guardado;
    var negocio = $('#contenedor-negocio').attr('data-negocio');
    if (negocio_visto != negocio) {
        localStorage.setItem('min-inicio-detalle', datetime);
        localStorage.setItem('hora-inicio-detalle', current_hora);
        localStorage.setItem('negocio-visto', negocio);
        $.ajax({
            url: '/registro/clic/',
            method: 'GET',
            data: {'negocio': negocio},
            success: function (response) {
                console.log(response.rc);
            }

        });
    } else {
        if (resta >= 2 || hora_guardada != current_hora) {
            localStorage.setItem('min-inicio-detalle', datetime);
            localStorage.setItem('hora-inicio-detalle', current_hora);
            localStorage.setItem('negocio-visto', negocio);
            $.ajax({
                url: '/registro/clic/',
                method: 'GET',
                data: {'negocio': negocio},
                success: function (response) {
                    console.log(response.rc);
                }

            });
        }
    }

});