from django.db import models
from imagekit.models import ProcessedImageField
from imagekit.processors import Transpose
from qrofind_web.utils import PathAndRename
from django.template.defaultfilters import slugify
from geoposition.fields import GeopositionField

path_and_rename_categorias = PathAndRename("categorias")
path_and_rename_servicios = PathAndRename("servicios")
path_and_rename_promociones = PathAndRename("promociones")
path_and_rename_negocios = PathAndRename("negocios")


class Categoria(models.Model):
    zona = models.ForeignKey('main.Zona', null=True, blank=True, related_name='categorias')
    nombre = models.CharField(max_length=30)
    slug = models.SlugField(max_length=60, editable=False)
    orden = models.IntegerField(default=0)
    banner = ProcessedImageField(max_length=255, blank=True, null=True,
                                 upload_to=path_and_rename_categorias,
                                 processors=[Transpose()])
    # inicio = models.BooleanField(default=False, help_text='Marque si quiere esta categoría en la página de inicio')

    def __str__(self):
        return "{}-{}".format(self.nombre,self.zona)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.nombre)
        return super(Categoria, self).save(*args, **kwargs)


class Subcategoria(models.Model):
    categoria = models.ForeignKey('negocios.Categoria', related_name='subcategorias')
    nombre = models.CharField(max_length=30)
    slug = models.SlugField(max_length=60, editable=False)
    orden = models.IntegerField(default=0)
    inicio = models.BooleanField(default=False, help_text='Marque si quiere esta subcategoría en la página de inicio')

    def __str__(self):
        return self.nombre

    def save(self, *args, **kwargs):
        self.slug = slugify(self.nombre)
        return super(Subcategoria, self).save(*args, **kwargs)

    def get_negocios_ordenados(self):
        return self.negocios.all().order_by('orden_subcategoria')

    def get_zona(self):
        return self.categoria.zona


class Servicio(models.Model):
    nombre = models.CharField(max_length=30)
    imagen = ProcessedImageField(max_length=255, blank=True, null=True,
                                 upload_to=path_and_rename_servicios,
                                 processors=[Transpose()])

    def __str__(self):
        return self.nombre


class Promocion(models.Model):
    negocio = models.ForeignKey('negocios.Negocio', related_name='promociones')
    titulo = models.CharField(max_length=25, blank=True)
    texto = models.TextField()
    imagen = ProcessedImageField(max_length=255, blank=True, null=True,
                                 upload_to=path_and_rename_promociones,
                                 processors=[Transpose()])
    fecha_inicio = models.DateField()
    fecha_fin = models.DateField()

    def __str__(self):
        return self.texto


class Negocio(models.Model):
    sub_categoria = models.ManyToManyField('negocios.Subcategoria', related_name='negocios')
    zona = models.ForeignKey('main.Zona', related_name='negocios')
    imagen_principal = ProcessedImageField(max_length=255, null=True,
                                           upload_to=path_and_rename_negocios,
                                           processors=[Transpose()])
    imagen_promocion = ProcessedImageField(max_length=255, null=True, blank=True,
                                           upload_to=path_and_rename_negocios,
                                           processors=[Transpose()])
    nombre = models.CharField(max_length=30)
    telefono1 = models.CharField(max_length=30, blank=True, null=True)
    telefono2 = models.CharField(max_length=30, blank=True, null=True)
    telefono3 = models.CharField(max_length=30, blank=True, null=True)
    telefono4 = models.CharField(max_length=30, blank=True, null=True)
    direccion = models.TextField(blank=True, null=True)
    horario = models.TextField(blank=True, null=True)
    pagina_web = models.URLField(max_length=200, blank=True, null=True)
    descripcion_corta = models.CharField(max_length=60,blank=True, null=True)
    descripcion = models.TextField(blank=True, null=True)
    position = GeopositionField(db_index=True, blank=True, null=True)
    # latitude = models.FloatField(db_index=True, blank=True, null=True)
    # longitude = models.FloatField(db_index=True, blank=True, null=True)
    email_contacto = models.CharField(max_length=50, blank=True, null=True)
    email_contacto2 = models.CharField(max_length=50, blank=True, null=True)
    email_contacto3 = models.CharField(max_length=50, blank=True, null=True)
    servicios = models.ManyToManyField('negocios.Servicio', blank=True)
    facebook_url = models.URLField(max_length=200, blank=True, null=True)
    twitter_url = models.URLField(max_length=200, blank=True, null=True)
    instagram_url = models.URLField(max_length=200, blank=True, null=True)
    yotutbe_url = models.URLField(max_length=200, blank=True, null=True)
    google_plus_url = models.URLField(max_length=200, blank=True, null=True)
    es_premium = models.BooleanField(default=False)
    promocionado = models.BooleanField(default=False)
    servicio_domicilio = models.BooleanField(default=False)
    orden = models.IntegerField(default=0, help_text='Orden para la promoción')
    orden_subcategoria = models.IntegerField(default=0, help_text='Orden para la subcategoria')
    cantidad_clic = models.IntegerField(editable=False, default=0, help_text='Solo para negocios no premium clics en ver teléfono')
    slug = models.SlugField(max_length=60, editable=False, null=True)

    def __str__(self):
        return self.nombre

    def save(self, *args, **kwargs):
        self.slug = slugify(self.nombre)
        return super(Negocio, self).save(*args, **kwargs)


class FotoNegocio(models.Model):
    texto = models.CharField(max_length=100, blank=True, null=True)
    imagen = ProcessedImageField(max_length=255, null=True,
                                 upload_to=path_and_rename_negocios,
                                 processors=[Transpose()])
    negocio = models.ForeignKey('negocios.Negocio', related_name="imagenes")
    orden = models.IntegerField(default=0)

    def __str__(self):
        return self.texto


class FotoNegocioAmplia(models.Model):
    texto = models.CharField(max_length=100, blank=True, null=True)
    imagen = ProcessedImageField(max_length=255, null=True,
                                 upload_to=path_and_rename_negocios,
                                 processors=[Transpose()])
    negocio = models.ForeignKey('negocios.Negocio', related_name="imagenes_amplias")
    orden = models.IntegerField(default=0)

    def __str__(self):
        return self.texto


class RegistroContato(models.Model):
    negocio = models.ForeignKey('negocios.Negocio')
    texto = models.CharField(max_length=500, blank=True, null=True)
    correo = models.CharField(max_length=50, blank=True, null=True)
    mensaje_de = models.CharField(max_length=100, blank=True, null=True)

    def __str__(self):
        return "contacto a {}".format(self.negocio)
