from django.conf.urls import include, url
from qrofind_web.negocios import views
urlpatterns = [
    url(r'^categoria/(?P<slug_categoria>.+)/$', views.negocios_categorias, name='negocios_categorias'),
    url(r'^detalle/negocio/(?P<slug_negocio>.+)/$', views.detalle_negocio, name='detalle_negocio'),
    url(r'^registro/clic/$', views.registro_clic_negocio, name='registro_clic_negocio'),
    url(r'^promociones/$', views.promociones, name='promociones'),
    url(r'^anunciate/$', views.anunciate, name='anunciate'),
    url(r'^inicio/anunciate/$', views.anunciate_inicio, name='anunciate_inicio'),
]
