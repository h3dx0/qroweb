from django.db import models
from django.contrib.auth.models import AbstractUser, UserManager
from easy_thumbnails.signals import saved_file
from easy_thumbnails.signal_handlers import generate_aliases_global
import binascii
import os
from imagekit.models import ProcessedImageField
from imagekit.processors import Transpose
from qrofind_web.utils import PathAndRename
from django.template.defaultfilters import slugify

saved_file.connect(generate_aliases_global)

path_and_rename_usuarios = PathAndRename("usuarios")
path_and_rename_banners = PathAndRename("banners")
path_and_rename_articulos = PathAndRename("articulos")
path_and_rename_fotos_articulos = PathAndRename("fotos_articulos")
path_and_rename_banner_zona = PathAndRename("banner_zona")

SECCIONES_BANNER = (
    (0, 'Promociones'),
    (1, 'Home Nacional'),
    (2, 'Home Lateral'),
    (3, 'Home Cintillo'),
    (4, 'Home Pie'),
    (5, 'Inicio Zonas Principal'),
    (6, 'Inicio Zonas Lateral'),
    (7, 'Inicio Zonas Banner Pie'),
    (8, 'Home Cintillo 1'),
)


class Usuario(AbstractUser):
    objects = UserManager()
    zona = models.OneToOneField('main.Zona', related_name='usuario', null=True)
    class Meta(AbstractUser.Meta):
        verbose_name = "User"
        verbose_name_plural = "Users"


class Token(models.Model):
    key = models.CharField(max_length=40, primary_key=True)
    user = models.OneToOneField(Usuario, related_name='auth_token')
    created = models.DateTimeField(auto_now_add=True)

    def save(self, *args, **kwargs):
        if not self.key:
            self.key = self.generate_key()
        return super(Token, self).save(*args, **kwargs)

    @staticmethod
    def generate_key():
        return binascii.hexlify(os.urandom(20)).decode()

    def __str__(self):
        return self.key


class Articulo(models.Model):
    titulo = models.CharField(max_length=30)
    imagen = ProcessedImageField(max_length=255, upload_to=path_and_rename_articulos,
                                 processors=[Transpose()])
    descripcion_corta = models.TextField()
    descripcion_larga = models.TextField()
    zona = models.ForeignKey('main.Zona', related_name='articulos')
    slug = models.SlugField(max_length=60, editable=False, unique=True)
    fecha = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.titulo

    def save(self, *args, **kwargs):
        self.slug = slugify(self.titulo)
        return super(Articulo, self).save(*args, **kwargs)



class FotoArticulo(models.Model):
    texto = models.CharField(max_length=100, blank=True, null=True)
    imagen = ProcessedImageField(max_length=255, null=True,
                                 upload_to=path_and_rename_fotos_articulos,
                                 processors=[Transpose()])
    articulo = models.ForeignKey('Articulo', related_name="imagenes")
    orden = models.IntegerField(default=0)

    def __str__(self):
        return self.texto


class Zona(models.Model):
    nombre = models.CharField(max_length=30)
    slug = models.SlugField(max_length=60, editable=False, unique=True)
    imagen = ProcessedImageField(max_length=255, null=True,
                                 upload_to=path_and_rename_banner_zona,
                                 processors=[Transpose()])
    facebook = models.CharField(max_length=50, null=True, blank=True)
    instagram = models.CharField(max_length=50, null=True, blank=True)
    orden = models.IntegerField(default=0)

    def __str__(self):
        return self.nombre

    def save(self, *args, **kwargs):
        self.slug = slugify(self.nombre)
        return super(Zona, self).save(*args, **kwargs)


class Banner(models.Model):
    nombre = models.CharField(max_length=50)
    zona = models.ForeignKey('main.Zona', related_name='banners')
    seccion = models.IntegerField(default=0, choices=SECCIONES_BANNER)
    link = models.URLField(max_length=200)
    orden = models.IntegerField(default=0)
    pagina_externa = models.BooleanField(default=False)
    imagen = ProcessedImageField(max_length=255, blank=True, null=True,
                                 upload_to=path_and_rename_banners,
                                 processors=[Transpose()])

    def __str__(self):
        return "{}-{}".format(self.zona, self.nombre)
