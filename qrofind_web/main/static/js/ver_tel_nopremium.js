/**
 * Created by h3dx0 on 14/10/16.
 */
$(function () {
    var currentdate = new Date();
    var datetime = currentdate.getMinutes();
    var minuto_guardado = localStorage.getItem('min-inicio');
    $('.tel-nopremiun').hide();
    $('.ver-telefono').on('click', function () {
        var negocio_visto = localStorage.getItem('negocio-visto-nopremiun');
        var negocio = $(this).attr('data-negocio');
        if (negocio != negocio_visto) {
            localStorage.setItem('min-inicio', datetime);
            localStorage.setItem('negocio-visto-nopremiun', negocio);

            $.ajax({
                url: '/registro/clic/',
                method: 'GET',
                data: {'negocio': negocio},
                success: function (response) {
                    console.log(response.rc);
                }

            });
        } else {
            if (datetime - minuto_guardado >= 2 || datetime - minuto_guardado < 0) {
                localStorage.setItem('min-inicio', datetime);
                localStorage.setItem('negocio-visto-nopremiun', negocio);
                $.ajax({
                    url: '/registro/clic/',
                    method: 'GET',
                    data: {'negocio': negocio},
                    success: function (response) {
                        console.log(response.rc);
                    }

                });
                // console.log('contar clic');
            }
        }

        var telefono = $(this).find('.tel-nopremiun');
        $(this).find('.ver-tel-btn').hide();
        telefono.show();

    });
});