/**
 * Created by h3dx0 on 16/12/16.
 */
$(function () {

    var tamanoPantalla = screen.width;
    if (tamanoPantalla < 750) {
        console.log('Verion movil');
        $('#menu-desktop').remove();
        $('#menu-movil').css('display', 'block');
    }
    if (tamanoPantalla >= 750) {
        console.log('Verion desktop');
        $('#menu-movil').remove();
        $('#menu-desktop').css('display', 'block');

    }
});