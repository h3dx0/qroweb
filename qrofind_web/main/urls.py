from django.conf.urls import include, url
from qrofind_web.main import views
urlpatterns = [
    url(r'^$', views.inicio_zonas, name='inicio_zonas'),
    url(r'^zona/(?P<slug_zona>.+)$', views.index, name='index'),
    url(r'^buscar$', views.buscar, name='buscar'),
    url(r'^promociones/generales', views.promociones_generales, name='promociones_generales'),
    url(r'^detalle/articulo/(?P<slug_articulo>.+)$', views.detalle_articulo, name='detalle_articulo'),
]
