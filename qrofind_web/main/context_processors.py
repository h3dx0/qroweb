from qrofind_web.main.models import *
from qrofind_web.negocios.models import Categoria
from qrofind_web.main.forms import *
from django.conf import settings
from django.core.mail import send_mail


def obtener_menu_cateogrias(request):
    slug_zona = 'default'

    if request.session.__contains__('slug_zona_activa'):
        slug_zona = request.session['slug_zona_activa']

        zona = Zona.objects.get(slug=slug_zona)
        categorias = Categoria.objects.filter(zona=zona).order_by('orden')[:8]
        categorias_restantes = Categoria.objects.filter(zona=zona).order_by('orden')[8:]
    else:
        zona = None
        categorias = None
        categorias_restantes = None

    return {'zona': zona, 'categorias': categorias, 'categorias_restantes': categorias_restantes}


def enviar_boletin(request):
    form_boletin = BoletinForm()
    mensaje = None
    if request.method == 'POST':
        form = BoletinForm(request.POST)
        if form.is_valid():
            nombre = form.cleaned_data['nombre']
            email = form.cleaned_data['email']
            zona = form.cleaned_data['zona']
            texto = 'De {} email: {} \n Zona: {} \n Deseo unirme al boletín'.format(nombre,
                                                            email,
                                                            zona)
            send_mail('Correo desde la web para boletín', texto, settings.DEFAULT_FROM_EMAIL,
                      ['pedro@tuzona.mx'], fail_silently=True)
            mensaje = "Se ha enviado su suscripción"

    return {'formulario': form_boletin, 'mensaje': mensaje}