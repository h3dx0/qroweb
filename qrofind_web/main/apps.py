from django.apps import AppConfig


class MainConfig(AppConfig):
    name = 'qrofind_web.main'

    def ready(self):
        import qrofind_web.main.signals
