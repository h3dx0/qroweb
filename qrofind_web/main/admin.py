from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .admin_forms import *
from django.utils.translation import ugettext, ugettext_lazy as _


class UsuarioAdmin(UserAdmin):
    form = MyUserChangeForm
    add_form = MyUserCreationForm
    fieldsets = (
        (None, {'fields': ('username', 'password', 'zona')}),
        (_('Personal info'), {'fields': ('first_name', 'last_name', 'email')}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser', 'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )

class BannerAdmin(admin.ModelAdmin):
    list_display = ['nombre', 'zona']

class FotoArticuloAdmin(admin.TabularInline):
    model = FotoArticulo

class ArticuloAdmin(admin.ModelAdmin):
    inlines = [
        FotoArticuloAdmin,
    ]
admin.site.register(Usuario, UsuarioAdmin)
admin.site.register(Zona)
admin.site.register(Articulo,ArticuloAdmin)
admin.site.register(Banner, BannerAdmin)
