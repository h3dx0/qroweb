from django.db.models import Q
from django.shortcuts import render
from qrofind_web.main.forms import *
from qrofind_web.negocios.utils import *


# Create your views here.

def inicio_zonas(request):
    zonas = Zona.objects.all().exclude(slug='default').order_by('orden')
    # zonas_restantes = Zona.objects.all().exclude(slug='default')[6:]
    articulos = Articulo.objects.all().order_by('-id')
    banner_principal = Banner.objects.filter(seccion=5)
    banners_laterales = Banner.objects.filter(seccion=6)
    banner_pie = Banner.objects.filter(seccion=7).order_by('orden')[:1]

    if banner_pie.count() > 0:
        banner_pie = banner_pie[0]
    context = {
        'zonas': zonas,
        # 'zonas_restantes': zonas_restantes,
        'articulos': articulos,
        'banner_laterales': banners_laterales,
        'banners_principales': banner_principal,
        'banner_pie': banner_pie
    }
    return render(request, 'inicio_zonas.html', context)


def promociones_generales(request):
    promociones = Promocion.objects.all()
    zonas = Zona.objects.all().exclude(slug='default').order_by('orden')[:5]
    zonas_restantes = Zona.objects.all().order_by('orden').exclude(slug='default')[6:]
    banner_pie = Banner.objects.filter(seccion=7).order_by('orden')[:1]
    if banner_pie.count() > 0:
        banner_pie = banner_pie[0]

    context = {
        'zonas': zonas,
        'zonas_restantes': zonas_restantes,
        'promociones': promociones,
        'banner_pie': banner_pie
    }
    return render(request, 'promociones_generales.html', context)


def index(request, slug_zona):
    banners_cintillo_nacional = None
    zona = Zona.objects.get(slug=slug_zona)
    # categorias = Categoria.objects.filter(zona=zona).all()

    negocios_premium = Negocio.objects.filter(zona=zona, es_premium=True).order_by('-id')[:4]
    subcategorias_inicio = Subcategoria.objects.filter(categoria__zona=zona, inicio=True).order_by('orden')[:2]
    negocios_primera_subcategoria = subcategorias_inicio[0].negocios.filter(promocionado=True)[:7]
    negocios_segunda_subcategoria = subcategorias_inicio[1].negocios.filter(promocionado=True)[:7]
    request.session['slug_zona_activa'] = slug_zona

    context = {
        'zona': zona,
        # 'categorias': categorias,
        'primera_subcategorias_inicio': subcategorias_inicio[0],
        'segunda_subcategorias_inicio': subcategorias_inicio[1],
        'negocios_primera_subcategoria': negocios_primera_subcategoria,
        'negocios_segunda_subcategoria': negocios_segunda_subcategoria,
        'negocios_premium': negocios_premium
    }
    return render(request, 'index.html', context)


def detalle_articulo(request, slug_articulo):
    articulo = Articulo.objects.get(slug=slug_articulo)
    zonas = Zona.objects.all().order_by('orden').exclude(slug='default')[:5]
    zonas_restantes = Zona.objects.all().order_by('orden').exclude(slug='default')[6:]
    banner_principal = Banner.objects.filter(seccion=5)
    banners_laterales = Banner.objects.filter(seccion=6)
    banner_pie = Banner.objects.filter(seccion=7).order_by('orden')[:1]
    if banner_pie.count() > 0:
        banner_pie = banner_pie[0]
    context = {
        'zonas': zonas,
        'zonas_restantes': zonas_restantes,
        'articulo': articulo,
        'banner_laterales': banners_laterales,
        'banners_principales': banner_principal,
        'banner_pie': banner_pie
    }
    return render(request, 'detalle_articulo.html', context)


def buscar(request):
    texto = request.GET.get('search')
    try:
        zona_activa = get_zona_activa(request)
        negocios = Negocio.objects.filter(zona=zona_activa).filter(Q(nombre__icontains=texto) |
                                                                   Q(descripcion__icontains=texto) |
                                                                   Q(descripcion_corta__icontains=texto) |
                                                                   Q(sub_categoria__nombre__icontains=texto) |
                                                                   Q(servicios__nombre__icontains=texto)).distinct()
    except Exception:
        negocios = Negocio.objects.filter(Q(nombre__icontains=texto) |
                                          Q(descripcion__icontains=texto) |
                                          Q(descripcion_corta__icontains=texto) |
                                          Q(sub_categoria__nombre__icontains=texto) |
                                          Q(servicios__nombre__icontains=texto)).distinct()
    context = {
            'negocios': negocios,
            'cantidad_negocios': negocios.count(),
            'texto': texto
        }
    return render(request, 'resultado_busqueda.html', context)
