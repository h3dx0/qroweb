__author__ = 'robe'
from django import template
import locale

register = template.Library()


@register.inclusion_tag('texto.html')
def texto(texto):
    texto_str = ""
    if texto is not None:
        texto_str = texto.replace("\n", "<br>")
    return {'texto': texto_str}
